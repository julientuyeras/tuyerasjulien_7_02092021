//Imports
const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/user');
const messageRoutes = require ('./routes/message');
const commentRoutes = require ('./routes/commentaire')
const path = require('path');
const cors = require('cors')

//Instantiate server
const app = express();


app.use(cors())

/*app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});*/

//BodyParser configuration
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

app.use('/images', express.static(path.join(__dirname, 'images')));

//Api routes
app.use('/api', userRoutes);
app.use('/api', messageRoutes);
app.use('/api', commentRoutes);


/* Helmet ( protection http ) 
prévenir contre les faille xss (injection) 
// ...is equivalent to this:
app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());
*/
const helmet = require("helmet");
app.use(helmet());
/* fin helmet */

// Start the Needle.sh SDK ( https://needle.sh ) 
/*
SQL injection,Reflected XSS,Command injection,Local File Inclusion (LFI),Server Side Request Forgery (SSRF),Security scanner protection,
Shellshock protection,Content Security Policy (CSP),Clickjacking protection,Browser XSS header,Mime sniffing header,Referrer policy header
*/
const needle_sdk = require('needle-sdk');
needle_sdk.start();
//fin needle 


// ration est un script 100% Vanilla Javascript de Rate Limiting / Bibliothèque de protection DDOS
const ration = require('ration.js');

/* Set Ration.js Options */
const rations = {
    maxRequestsPerTimeFrame: 600,
    timeFrameInSeconds: 30,
    removeRecordsAfter: (1000 * 60 * 5),
    dropConnections: false
  };
  


module.exports = app;