# Installation

## *configuration DataBase*

- Allez dans le fichier config, puis config.json
- renseignez les identifiants
    host
    user
    password

- Ouvrez le terminal sur le dossier Backend et écrivez la commande suivante (vous devez au préalable avoir mySQL sur votre ordinateur):
    'sequelize db:create'
- Puis la commande suivante:
    'sequelize db:migrate'

## *Backend*

- Ouvrez le terminal sur dossier Backend et écrivez la commande:
    'npm install'
- Une fois avoir installé npm, écrivez la commande:
    'nodemon server'
- Puis, gardez ce terminal ouvert

## *Frontend*

- Ouvrez un second terminal en vous mettant sur le dossier "frontend" et écrivez la commande:
    'npm install'
- Une fois cela fait, écrivez la commande:
    'npm run serve'
- et gardez ce deuxième terminal ouvert

## *Application*

    Ouvrez l'application sur http://localhost:8080/
